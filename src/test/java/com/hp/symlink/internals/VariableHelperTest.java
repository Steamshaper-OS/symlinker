package com.hp.symlink.internals;

import java.io.File;
import java.io.StringWriter;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.steamshaper.symlink.Constants.GENERATE_DEFAULT;
import org.steamshaper.symlink.internals.ConfigScanner;
import org.steamshaper.symlink.internals.VariableHelp;
import org.steamshaper.symlink.utils.Config;
import org.steamshaper.symlink.utils.Log;

public class VariableHelperTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	//@Test
	public void testGetVariableWithin() {
		 /*  first, get and initialize an engine  */
        VelocityEngine ve = new VelocityEngine();
        ve.init();
        /*  next, get the Template  */
        Template t = ve.getTemplate( "velocitytest.tpl" );
        /*  create a context and add data */
        VelocityContext context = new VelocityContext();
        context.put("test", ConfigScanner.readUserSettings(GENERATE_DEFAULT.NO));
        /* now render the template into a StringWriter */
        StringWriter writer = new StringWriter();
        t.merge( context, writer );
        /* show the World */
        System.out.println( writer.toString() );  
	}
	
	@Test
	public void testInterpolate() {
		Config.init(new String[]{});
		Log.wrappedDebug(VariableHelp.me.interpolateMarkerConfig(new File("D:/Workspaces/sym-link/testArchetype/kitchensink/src/main/extfs/conf/sym-link.marker.default.json")));
		//Log.wrappedDebug(VariableHelp.me.interpolate(H.fsTools.getResource("smlink-settings.json")));
	}

}
