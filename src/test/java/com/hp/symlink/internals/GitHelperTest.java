package com.hp.symlink.internals;

import java.net.Proxy.Type;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.api.errors.InvalidRemoteException;
import org.eclipse.jgit.api.errors.TransportException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.steamshaper.symlink.config.SymLinkProxyConf;
import org.steamshaper.symlink.internals.GitHelper;

public class GitHelperTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	//@Test
	public void testCloneRemote() throws InvalidRemoteException, TransportException, GitAPIException {
		GitHelper gh =  new GitHelper(true);
		Map<String,SymLinkProxyConf> conf =  new HashMap<>();
				SymLinkProxyConf smconf = new SymLinkProxyConf();
		smconf.setPort(3128);
		smconf.setType(Type.HTTP);
		smconf.setAddress("127.0.0.1");
				conf.put("gitscm.hpfactory.it", smconf);
		gh.addRoutes(conf);
		
		
		gh.cloneRemote(null, null,true);
	}

}
