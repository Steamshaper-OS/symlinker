/**
 * 
 */
package com.hp.symlink.internals;

import static org.junit.Assert.fail;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineFactory;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.steamshaper.symlink.internals.LinkBuilder;

/**
 * @author dfiungo
 *
 */
public class LinkBuilderTest {

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link org.steamshaper.symlink.internals.LinkBuilder#createSymbolicLink(java.io.File, java.io.File)}.
	 * @throws IOException 
	 */
	//@Test
	public void testCreateSymbolicLinkFileFile() throws IOException {
		LinkBuilder lb =  new LinkBuilder();
		File src = new File("D:/symLinkPlayground/src");
		File dst = new File("D:/symLinkPlayground/linkDest");
		if(dst.exists()){
			dst.delete();
		}
		lb.createSymbolicLink(src, dst);
		
		
		String newFileName = "/touch-"+System.currentTimeMillis()+".txt";
		File newFileOnSrc = new File(src.getAbsolutePath()+newFileName);
		newFileOnSrc.createNewFile();
		File newFileOnDest = new File(dst.getAbsolutePath()+newFileName);
		if(!newFileOnDest.exists()){
			fail("Unable to find new file ["+newFileOnDest.getAbsolutePath()+"]");
		}
	}

	/**
	 * Test method for {@link org.steamshaper.symlink.internals.LinkBuilder#createSymbolicLink(java.lang.String, java.lang.String)}.
	 */
//	@Test
	public void testCreateSymbolicLinkStringString() {
		
	//	fail("Not yet implemented");
	}
	
	//@Test
	public void scriptingEngines(){
		ScriptEngineManager manager = new ScriptEngineManager();
	    List<ScriptEngineFactory> factoryList = manager.getEngineFactories();
	    for (ScriptEngineFactory factory : factoryList) {
	    	  System.out.println(factory.getEngineName());
	          System.out.println(factory.getEngineVersion());
	          System.out.println(factory.getLanguageName());
	          System.out.println(factory.getLanguageVersion());
	          System.out.println(factory.getExtensions());
	          System.out.println(factory.getMimeTypes());
	          System.out.println(factory.getNames());
	          System.out.println("--------------------------------------------");
	    }
        ScriptEngine engine = manager.getEngineByName ("js");
        String script = "print ('www.java2s.com')";
        try {
            engine.eval (script);
        } catch (ScriptException e) {
            e.printStackTrace();
        }
	}

}
