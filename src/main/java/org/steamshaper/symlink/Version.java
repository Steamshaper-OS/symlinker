package org.steamshaper.symlink;

import java.util.StringTokenizer;

public class Version {

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		if (major != null)
			builder.append(major).append(".");
		if (minor != null)
			builder.append(minor).append(".");
		if (rev != null)
			builder.append(rev);
		if (name != null)
			builder.append(" - ").append(name);
		return builder.toString();
	}

	private String major="0";
	private String minor = "0";
	private String rev = "0";
	private String name="";
	
	public Version(String major, String minor, String rev, String name) {
		this.major = major;
		this.minor =  minor;
		this.rev = rev;
		this.name= name;
	}
	
	
	public Version(String mavenVersion,String build,String name){
		StringTokenizer st = new StringTokenizer(mavenVersion, ".");
		if(st.hasMoreTokens()){
			this.major=st.nextToken();
			if(st.hasMoreTokens()){
				this.minor=st.nextToken();
			}
		}
		this.rev="b"+build;
		this.name=name;
	}
	public String getMajor() {
		return major;
	}
	public void setMajor(String major) {
		this.major = major;
	}
	public String getMinor() {
		return minor;
	}
	
	public String getVersionNumber() {
		StringBuilder builder = new StringBuilder();
		builder.append(major).append(".").append(minor).append(".").append(rev);
		return builder.toString();
	}
	
	
}
