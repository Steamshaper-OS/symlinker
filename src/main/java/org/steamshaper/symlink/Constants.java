package org.steamshaper.symlink;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Properties;
import java.util.regex.Pattern;

import org.apache.commons.io.FilenameUtils;

public abstract class Constants {

	public enum GENERATE_DEFAULT {
		IF_NOT_EXISTS, NO
	}

	private static final String DEFAULT_DESTINATION_DIR = "workspace";

	private Constants() {

	}

	public static final String DEFAULT_JOB_CONFIG_FILE_NAME_PREFIX = "sym-link-job";
	private static final String DEFAULT_MARKER_PREFIX = "marker";

	public static final String DEFAULT_PROFILE_NAME = "default";

	public static final String DEFAULT_CONFIG_FILE_EXT = "json.smlink";

	private static final String DEFAULT_MARKER_FILE_NAME = DEFAULT_MARKER_PREFIX + "." + DEFAULT_PROFILE_NAME + "."
			+ DEFAULT_CONFIG_FILE_EXT;

	private final static List<String> DEFAULT_SOURCE_DIR;

	// private static final Pattern profileName = Pattern
	// .compile("([A-Za-z0-9])+");

	private static final Version SYMLINK_VERSION;

	public static final String[] LOGO;

	static {
		// List<String> defTemp = new ArrayList<>(1);
		DEFAULT_SOURCE_DIR = null;// Collections.unmodifiableList(defTemp);
		// defTemp.add(FilenameUtils.separatorsToUnix(System.getProperty("user.dir")));

		Properties prop = new Properties();
		ClassLoader loader = Thread.currentThread().getContextClassLoader();
		InputStream inputStream = null;
		try {
			inputStream = loader.getResourceAsStream("symlink.properties");
			prop.load(inputStream);
		} catch (IOException e) {
		} finally {
			try {
				inputStream.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		SYMLINK_VERSION = new Version(prop.getProperty("symlink.version"), prop.getProperty("symlink.build.number"),
				"Link this!");

		LOGO = new String[29];
		LOGO[0] = "                 _";
		LOGO[1] = "             ,.-\" \"-.,";
		LOGO[2] = "            /   ===   \\";
		LOGO[3] = "           /  =======  \\";
		LOGO[4] = "        __|  (o)   (0)  |__ ";
		LOGO[5] = "       / _|    .---.    |_ \\";
		LOGO[6] = "      | /.----/ O O \\----.\\ |";
		LOGO[7] = "       \\/     |     |     \\/ ";
		LOGO[8] = "       |                        ____             __   _      __";
		LOGO[9] = "       |                   |   / __/_ ____ _    / /  (_)__  / /_____ ____";
		LOGO[10] = "       |                   |  _\\ \\/ // /  ' \\  / /__/ / _ \\/  '_/ -_) __/";
		LOGO[11] = "       _\\   -.,_____,.-   /_ /___/\\_, /_/_/_/ /____/_/_//_/_/\\_\\\\__/_/   ";
		LOGO[12] = "   ,.-\"  \"-.,_________,.-\"  \"-.,   /_/                     ";
		LOGO[13] = "  /          |       |          \\";
		LOGO[14] = " |           l.     .l           |";
		LOGO[15] = " |            |     |            |";
		LOGO[16] = " l.           |     |           .l";
		LOGO[17] = "  |           l.   .l           | \\, ";
		LOGO[18] = "  l.           |   |           .l   \\,";
		LOGO[19] = "   |           |   |           |      \\,";
		LOGO[20] = "   l.          |   |          .l        |";
		LOGO[21] = "    |          |   |          |         |";
		LOGO[22] = "    |          |---|          |         |";
		LOGO[23] = "    |          |   |          |         |";
		LOGO[24] = "    /\"-.,__,.-\"\\   /\"-.,__,.-\"\\\"-.,_,.-\"\\";
		LOGO[25] = "   |            \\ /            |         |";
		LOGO[26] = "   |             |             |         |";
		LOGO[27] = "    \\__|__|__|__/ \\__|__|__|__/ \\_|__|__/";
		LOGO[28] = ""; 
	}

	public static File getDefaultJobConfigFile() {
		return new File(getJobConfigFileName());
	}

	public static Pattern getMarkerFilterRegExPattern() {
		Pattern out = Pattern.compile(DEFAULT_MARKER_PREFIX + "\\.?[a-z]*\\." + DEFAULT_CONFIG_FILE_EXT,
				Pattern.CASE_INSENSITIVE);
		return out;
	}

	public static Pattern getMarkerFilterRegExPattern(String profile) {
		Pattern out = Pattern.compile(DEFAULT_MARKER_PREFIX + "\\.?" + profile + "\\." + DEFAULT_CONFIG_FILE_EXT,
				Pattern.CASE_INSENSITIVE);
		return out;
	}

	public static Pattern getDirFilterRegExPattern() {
		Pattern out = Pattern.compile("[^.]+", Pattern.CASE_INSENSITIVE);
		return out;
	}

	public static String getJobConfigFileName() {
		return DEFAULT_JOB_CONFIG_FILE_NAME_PREFIX + "." + DEFAULT_CONFIG_FILE_EXT;
	}

	public static String getDefaultDestDir() {

		return DEFAULT_DESTINATION_DIR;
	}

	public static Version getVersion() {
		return SYMLINK_VERSION;
	}

	public static List<String> getDefaultSouceDirs() {
		return DEFAULT_SOURCE_DIR;
	}

	public static String getHomeDirectory() {
		return FilenameUtils.separatorsToUnix(System.getProperty("user.dir"));
	}

	public static String getDefaultMarkerFileName() {
		return DEFAULT_MARKER_FILE_NAME;
	}

	public static String getMarkerFileName(String profile) {
		return DEFAULT_MARKER_PREFIX + "." + profile + "." + DEFAULT_CONFIG_FILE_EXT;
	}

}
