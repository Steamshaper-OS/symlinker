package org.steamshaper.symlink;

import java.io.File;
import java.io.IOException;
import java.net.Proxy;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.eclipse.jgit.transport.HttpTransport;
import org.eclipse.jgit.transport.http.HttpConnection;
import org.eclipse.jgit.transport.http.JDKHttpConnectionFactory;
import org.eclipse.jgit.util.HttpSupport;
import org.steamshaper.symlink.config.SymLinkConfig;
import org.steamshaper.symlink.config.SymLinkMarker;
import org.steamshaper.symlink.utils.Config;
import org.steamshaper.symlink.utils.JSON;
import org.steamshaper.symlink.utils.Log;
import org.steamshaper.symlink.utils.Log.Verbosity;

public class SymLinker {

	public static void main(String[] args) throws IOException {
		Log.logLogo();

		CliArgs cliArgs = Config.init(args);
		if (cliArgs.isHelp()) {
			Config.printUsage();
			return;
		}

		if(cliArgs.isVerbose()){
			Log.setVerbosity(Verbosity.DEBUG);
		}else{
			Log.setVerbosity(Verbosity.INFO);
		}
		
		if (cliArgs.isDebug()) {
			Log.setVerbosity(Verbosity.DEBUG);
		}

		if (cliArgs.isGenerateJsonSchema()) {
			FileUtils.write(
					new File(FilenameUtils.concat(
							System.getProperty("user.dir"),
							SymLinkConfig.class.getSimpleName()
									+ ".schema.json")),
					JSON.help.generateSchema(SymLinkConfig.class),"UTF-8");

			FileUtils.write(
					new File(FilenameUtils.concat(
							System.getProperty("user.dir"),
							SymLinkMarker.class.getSimpleName()
									+ ".schema.json")),
					JSON.help.generateSchema(SymLinkMarker.class),"UTF-8");
			return;

		}

		if (cliArgs.isGenerateMarker()) {
			File marker = new File(Constants.getDefaultMarkerFileName());

			List<String> profiles = new ArrayList<>();

			if (cliArgs.getProfiles() != null && cliArgs.getProfiles().size()>0 ) {
				profiles.addAll(cliArgs.getProfiles());
			}else{
				profiles.add(Constants.DEFAULT_PROFILE_NAME);
			}

			for (String profile : profiles) {
				if (!marker.exists()) {

					SymLinkMarker newMarker = new SymLinkMarker();
					newMarker.setOverwriteIfExist(false);

					newMarker.setDestinationPath("destinationPath");
					newMarker.setRename("targetFolderName");
					JSON.help.writeConfig(newMarker,
							System.getProperty("user.dir"), profile, false);
				}
			}
			return;

		}

		// Log.wrappedDebug("User directory [%s]",
		// System.getProperty("user.dir"));
		File jobFile = null;
		if (StringUtils.isNoneBlank(cliArgs.getJobFilePath())) {
			jobFile = new File(cliArgs.getJobFilePath());
			if (!jobFile.exists() || jobFile.canRead()) {
				if (!cliArgs.isCreateJobConfigFile()) {
					Log.error("The file [%s] don't exist or isn't readable!",
							cliArgs.getJobFilePath());
				} else {
					Log.info("A default file will be created!");
				}
				return;
			}
		}

		SymLinkConfig cfg = Config.get.jobConfiguration();

		if (cliArgs.isCreateJobConfigFile()) {
			return;
		}
		
		HttpTransport.setConnectionFactory(new JDKHttpConnectionFactory(){

			@Override
			public HttpConnection create(URL url, Proxy proxy) throws IOException {
				// TODO Auto-generated method stub
				HttpConnection create = super.create(url, proxy);
				create.setConnectTimeout(10000);
				create.setReadTimeout(10000);
				HttpSupport.disableSslVerify(create);
				return create;
			}
			
		});
		
		SymLinkRunnable symLinker = new SymLinkRunnable(cliArgs.getProfiles());

		if (cfg != null) {

			Thread worker = new Thread(symLinker);
			worker.setDaemon(Boolean.FALSE);
			worker.setName("Worker thread!");

			worker.start();
		} else {
			Log.error("Unable to read Job Configuration!");
		}

	}
}
