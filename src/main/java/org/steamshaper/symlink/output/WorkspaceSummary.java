package org.steamshaper.symlink.output;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class WorkspaceSummary {
	
	
	private Date timestamp =  new Date();
	
	private List<ProjectSummary> project = new ArrayList<>();

	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	public List<ProjectSummary> getProject() {
		return project;
	}

	public void setProject(List<ProjectSummary> project) {
		this.project = project;
	}

}
