package org.steamshaper.symlink.internals.extractor;

import org.apache.commons.lang3.StringUtils;
import org.steamshaper.symlink.internals.Protocols;

public class ProtocolExtractor implements IExtractor<Protocols,String> {

	@Override
	public Protocols extract(String input) {
		if(StringUtils.isBlank(input)){
			return Protocols.NOT_SPECIFIED;
		}
		if(input.contains("://")){
			return Protocols.valueOf(StringUtils.upperCase(input.substring(0, input.indexOf(':')-1)));
		}else{
			return Protocols.NOT_SPECIFIED;
		}
		
	}



}
