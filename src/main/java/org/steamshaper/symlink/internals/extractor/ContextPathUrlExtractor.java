package org.steamshaper.symlink.internals.extractor;

import org.apache.commons.lang3.StringUtils;
import org.steamshaper.symlink.utils.Log;

public class ContextPathUrlExtractor implements IExtractor<String, String> {

	private static final String PROTOCOL_SEPARATOR = "://";

	@Override
	public String extract(final String input) {
		String output = "";
		if(StringUtils.isNotBlank(input)){
			String temp = input;
			Log.debug("Parsing  proxy conf PHASE 0 [%s]",temp);
			if(input.contains(PROTOCOL_SEPARATOR)){
				
				//removing protocol string es http://pippo.it ->pippo.it
				temp= input.substring(temp.indexOf(PROTOCOL_SEPARATOR));
				Log.debug("Parsing  proxy conf PHASE 1 [%s] Remove protocol!",temp);
				
			}
			if(temp.contains("/")){
				temp =  temp.substring(temp.indexOf('/'));
				Log.debug("Parsing  proxy conf PHASE 1 [%s] Remove context path!",temp);
			}
			
			output =  temp;
		}
		
		
		
		
		return output;
	}

}
