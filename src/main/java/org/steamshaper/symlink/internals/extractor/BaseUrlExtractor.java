package org.steamshaper.symlink.internals.extractor;

import org.apache.commons.lang3.StringUtils;
import org.steamshaper.symlink.utils.Log;

public class BaseUrlExtractor implements IExtractor<String, String> {

	private static final String PROTOCOL_SEPARATOR = "://";

	@Override
	public String extract(final String input) {
		String output = "";
		if(StringUtils.isNotBlank(input)){
			String temp = input;
			if(input.contains(PROTOCOL_SEPARATOR)){
				
				//removing protocol string es http://pippo.it ->pippo.it
				temp= input.substring(temp.indexOf(PROTOCOL_SEPARATOR));
				Log.debug("Parsing  proxy conf [%s] Remove protocol!",temp);
				
			}
			if(temp.contains("/")){
				temp =  temp.substring(0, temp.indexOf('/')-1);
				Log.debug("Parsing  proxy conf [%s] Remove context path!",temp);
			}
			
			output =  temp;
		}
		
		
		
		
		return output;
	}

}
