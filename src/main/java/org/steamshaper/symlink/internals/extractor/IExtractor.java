package org.steamshaper.symlink.internals.extractor;

public interface IExtractor<O,I> {

	
	public O extract(I input);
	
}
