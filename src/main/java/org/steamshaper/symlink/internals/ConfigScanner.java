package org.steamshaper.symlink.internals;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.IOFileFilter;
import org.apache.commons.io.filefilter.RegexFileFilter;
import org.steamshaper.symlink.Constants;
import org.steamshaper.symlink.Constants.GENERATE_DEFAULT;
import org.steamshaper.symlink.config.SymLinkConfig;
import org.steamshaper.symlink.config.SymLinkMarker;
import org.steamshaper.symlink.config.SymLinkProfileConfig;
import org.steamshaper.symlink.config.SymLinkProjectConf;
import org.steamshaper.symlink.config.SymLinkProxyConf;
import org.steamshaper.symlink.config.external.SymLinkExternalSettings;
import org.steamshaper.symlink.utils.H;
import org.steamshaper.symlink.utils.JSON;
import org.steamshaper.symlink.utils.Log;

public class ConfigScanner {

	private ConfigScanner() {

	}

	public static SymLinkConfig readJobConfig(File configFile,
			GENERATE_DEFAULT generate_DEFAULT) throws MissingConfigurationException {
		File cfg = configFile;
		if (configFile == null || !configFile.exists()) {

			cfg = Constants.getDefaultJobConfigFile();
			if (GENERATE_DEFAULT.IF_NOT_EXISTS.equals(generate_DEFAULT) && !cfg.exists()) {
				try {
					SymLinkConfig toWrite = new SymLinkConfig();
					SymLinkProfileConfig defaultProfile = new SymLinkProfileConfig();
					defaultProfile.setDestinationPath(Constants
							.getDefaultDestDir());
					defaultProfile.setMarkerFile(Constants
							.getDefaultMarkerFileName());
					defaultProfile.setSourceRoots(Constants
							.getDefaultSouceDirs());
					
					Log.toBeRemoved();
					//TOBE REMOVED
					SymLinkProxyConf defaultProxy = new SymLinkProxyConf();
					defaultProxy.setAddress("http://arya.stark.hp");
					defaultProxy.setPort(3128);
					toWrite.setDefaultProxy(defaultProxy);
					
					defaultProfile.setDestinationPath("ws");
					
					Map<String, SymLinkProjectConf> hashMap = new HashMap<String, SymLinkProjectConf>();
					SymLinkProjectConf projectKS = new SymLinkProjectConf();
					projectKS.setCheckoutFolder("htm-kitchensink");
					projectKS.setGitBranch("master");
					projectKS.setGitUrl("http://gitscm.hpfactory.it/frameworks-and-libs/htm-kitchensink.git");
					projectKS.setProjectName("KitchenSink");
					hashMap.put("htm-kitchensink",projectKS);
					defaultProfile.setProjects(hashMap);
					//TOBE REMOVED ---END

					
					
					
					toWrite.getProfiles().put(Constants.DEFAULT_PROFILE_NAME,
							defaultProfile);
					File cfgFile = JSON.help.writeConfig(toWrite,
							System.getProperty("user.dir"),
							Constants.DEFAULT_PROFILE_NAME, true);
					Log.info("Config file %s written!", cfgFile.getName());
					return toWrite;
				} catch (IOException e) {
					// TODO Auto-generated catch block
					cfg.delete();
					e.printStackTrace();
				}
			}

		}else{
			Log.info("Config file %s written!", cfg.getName());
		}
		if (cfg.exists()) {
			return JSON.help.readJobConfig(cfg);
		} else {
			throw new MissingConfigurationException(cfg.getAbsolutePath());
		}
	}

	public static SymLinkExternalSettings readUserSettings(
			GENERATE_DEFAULT ifNotExists) {
		File conf = H.fsTools.getResource("smlink-settings.json");

		
		return JSON.help.readExternalConfig(conf,
				Constants.GENERATE_DEFAULT.IF_NOT_EXISTS);
	}

	public static List<SymLinkMarker> findMarkers(RunContext runCtx) {
		List<SymLinkMarker> output = new ArrayList<>();
		IOFileFilter fileFilter = new RegexFileFilter(
				Constants.getMarkerFilterRegExPattern(runCtx.getProfileName()));
		IOFileFilter dirFilter = new RegexFileFilter(
				Constants.getDirFilterRegExPattern());

		List<File> cfgFiles = new ArrayList<File>();
		for (File cfgFile : runCtx.getScanPaths()) {

			for (File file : FileUtils
					.listFiles(cfgFile, fileFilter, dirFilter)) {
				File parentFile = file.getParentFile();
				if (Files.isSymbolicLink(parentFile.toPath())) {
					Log.debug("Skip %s because is a symbolic link",
							H.me.removeHomeDir(parentFile.getAbsolutePath()));
				} else {
					cfgFiles.add(file);
				}
			}
		}

		// Filter duplicate
		HashMap<String, File> hashMap = new HashMap<>(cfgFiles.size());

		for (File cfg : cfgFiles) {
			String absolutePath = cfg.getAbsolutePath();
			if (hashMap.containsKey(absolutePath)) {
				Log.warning("Found duplicate marker %s", cfg.getAbsolutePath());
			} else {
				Log.debug("Marker found [%s]", H.me.removeHomeDir(absolutePath));
				hashMap.put(absolutePath, cfg);
			}
		}

		for (File cfg : hashMap.values()) {
			try {
				output.add(JSON.help.readMarker(cfg));
			} catch (Exception e) {
				// FIXME
			}
		}
		
		Log.info("Marker found (%s)", output.size());
		return output;

	}

}
