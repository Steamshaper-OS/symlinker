package org.steamshaper.symlink.internals;

public class MissingConfigurationException extends Exception {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 7191809838417895590L;

	public MissingConfigurationException(String message) {
		super("Unable to find configuration file! Check your working directory! "
				+ (message != null ? message : ""));
	}

	public MissingConfigurationException() {
		this(null);
	}

}
