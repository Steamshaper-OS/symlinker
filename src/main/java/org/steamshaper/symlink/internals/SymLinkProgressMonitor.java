package org.steamshaper.symlink.internals;

import org.eclipse.jgit.lib.ProgressMonitor;
import org.steamshaper.symlink.utils.Log;

public class SymLinkProgressMonitor implements ProgressMonitor {
	int totalWork = -1;

	long lastUpdate = -1;

	long updateEvery = 100;

	@Override
	public void update(int completed) {

		long currentTimeMillis = System.currentTimeMillis();
		if (currentTimeMillis > (lastUpdate + updateEvery)) {
			Log.infoInline("%s", "*");
			lastUpdate = currentTimeMillis;
		}

	}

	@Override
	public void start(int totalTasks) {
		Log.infoInline("%s", "|");

	}

	@Override
	public boolean isCancelled() {
		return false;
	}

	@Override
	public void endTask() {
		Log.infoInline("%s%n", " | DONE");

	}

	@Override
	public void beginTask(String title, int totalWork) {
		this.totalWork = totalWork;
		Log.infoInline("%s >>>", title);

	}
}