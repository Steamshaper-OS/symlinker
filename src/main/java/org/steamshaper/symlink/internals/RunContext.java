package org.steamshaper.symlink.internals;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.steamshaper.symlink.Constants;
import org.steamshaper.symlink.config.SymLinkConfig;
import org.steamshaper.symlink.config.SymLinkProfileConfig;
import org.steamshaper.symlink.utils.Log;

public class RunContext {

	private SymLinkProfileConfig symLinkJobConfig;
	private SymLinkConfig jobConfiguration;
	private String profileName;

	private RunContext() {

	}

	public static RunContext build(SymLinkConfig jobConfiguration,
			SymLinkProfileConfig profile, String profileName) {
		RunContext runContext = new RunContext();
		runContext.setJobConfiguration(jobConfiguration);
		runContext.setCurrentProfile(profile);
		runContext.setProfileName(profileName);
		return runContext;
	}

	private void setProfileName(String profileName) {
		this.profileName = profileName;

	}

	protected void setCurrentProfile(SymLinkProfileConfig symLinkJobConfig) {
		this.symLinkJobConfig = symLinkJobConfig;
	}

	public SymLinkProfileConfig getCurrentProfile() {
		return symLinkJobConfig;
	}

	public SymLinkProfileConfig getSymLinkJobConfig() {
		return symLinkJobConfig;
	}

	public String getProfileName() {
		return profileName;
	}

	public List<File> getScanPaths() {
		List<File> output = new ArrayList<>(1);
		List<String> sourceRoots = new ArrayList<>();
		if (getJobConfiguration().getSources() != null && !getJobConfiguration().getSources().isEmpty()) {
			sourceRoots.addAll(getJobConfiguration().getSources());
		}
		if (sourceRoots.isEmpty()) {
			sourceRoots.add(Constants.getHomeDirectory());
		}
		for (String pathStr : sourceRoots) {
			output.add(createFileObj(pathStr));

		}
		return output;
	}

	private File createFileObj(String pathStr) {

		File output = new File(pathStr);
		String workDirectory = this.jobConfiguration.getWorkDirectory();
		if (!output.isAbsolute() && StringUtils.isNotBlank(workDirectory)) {
			output = new File(FilenameUtils.concat(workDirectory, pathStr));
		}
		if (!output.exists()) {
			Log.error("The path [%s] don't exist!!!", output.getAbsolutePath());
		}
		return output;
	}

	public SymLinkConfig getJobConfiguration() {
		return jobConfiguration;
	}

	protected void setJobConfiguration(SymLinkConfig jobConfiguration) {
		this.jobConfiguration = jobConfiguration;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("RunContext [");
		if (symLinkJobConfig != null)
			builder.append("symLinkJobConfig=").append(symLinkJobConfig)
					.append(", ");
		if (jobConfiguration != null)
			builder.append("jobConfiguration=").append(jobConfiguration);
		builder.append("]");
		return builder.toString();
	}

}
