package org.steamshaper.symlink.internals;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.ProxySelector;
import java.net.SocketAddress;
import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.steamshaper.symlink.config.SymLinkProxyConf;
import org.steamshaper.symlink.internals.extractor.BaseUrlExtractor;
import org.steamshaper.symlink.internals.extractor.IExtractor;
import org.steamshaper.symlink.utils.Log;

public class ConfigurableProxySelector extends ProxySelector {

	// private static final IExtractor<Protocols, String> protocolExtractor =
	// new ProtocolExtractor();
	private static final IExtractor<String, String> baseUriExtractor = new BaseUrlExtractor();
	// private static final IExtractor<String, String> ctxPathExtractor = new
	// ContextPathUrlExtractor();

	private final ProxySelector delegate = ProxySelector.getDefault();

	private Map<String, List<SymLinkProxyConf>> proxyIndex = new HashMap<>();

	private SymLinkProxyConf defaultProxy = null;

	public void addRoute(Map<String, SymLinkProxyConf> proxyMap) {
		for (Entry<String, SymLinkProxyConf> entry : proxyMap.entrySet()) {

			String baseURL = baseUriExtractor.extract(entry.getKey())
					.toLowerCase();
			addToIndex(baseURL, entry.getValue());
			Log.info("Proxy Route [%s] --> [%s]", baseURL,entry.getValue());

		}

	}

	private void addToIndex(String base, SymLinkProxyConf conf) {
		List<SymLinkProxyConf> baseEntry = proxyIndex.get(base);

		if (baseEntry == null) {
			baseEntry = new ArrayList<>();
			proxyIndex.put(base, baseEntry);
		}

		Proxy proxy = createProxy(conf);
		if (!baseEntry.contains(proxy)) {
			baseEntry.add(conf);
		}

	}

	private Proxy createProxy(SymLinkProxyConf conf) {
		return new Proxy(conf.getType(), InetSocketAddress.createUnresolved(
				conf.getAddress(), conf.getPort()));
	}

	@Override
	public List<Proxy> select(URI uri) {
		// Filter the URIs to be proxied

		List<Proxy> doSelection = doSelection(uri);
		Log.debug("Proxy for %s : %s", uri.toString(),doSelection);
		return doSelection;
	}

	private List<Proxy> doSelection(URI uri) {
		List<SymLinkProxyConf> plist = proxyIndex.get(baseUriExtractor
				.extract(uri.getHost()));
		if (plist != null && plist.size() > 0) {
			List<Proxy> list = new ArrayList<Proxy>(plist.size());
			for (SymLinkProxyConf conf : plist) {
				list.add(createProxy(conf));
			}
			return list;
		} else if (defaultProxy != null) {
			return Arrays.asList(createProxy(defaultProxy));
		} else {
			return delegate == null ? Arrays.asList(Proxy.NO_PROXY) : delegate
					.select(uri);
		}
	}

	@Override
	public void connectFailed(URI uri, SocketAddress sa, IOException ioe) {
		if (uri == null || sa == null || ioe == null) {
			throw new IllegalArgumentException("Arguments can't be null.");
		}
	}

	public void addDefaultRoute(SymLinkProxyConf defaultProxy) {
		this.defaultProxy = defaultProxy;

	}

}