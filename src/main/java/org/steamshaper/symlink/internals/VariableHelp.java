package org.steamshaper.symlink.internals;

import java.io.File;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.steamshaper.symlink.utils.Config;
import org.steamshaper.symlink.utils.H;
import org.steamshaper.symlink.utils.Log;

public class VariableHelp {

	public static final VariableHelp me = new VariableHelp();

	private Map<String, VelocityEngine> veMap = new HashMap<>();

	private VariableHelp() {

	}

	public String interpolate(Map<String, Object> extraArgs, File targetFile) {

		VelocityContext context = buildContext(extraArgs, true);

		Template tpl = getTemplateForFile(targetFile);
		StringWriter strWriter = new StringWriter();
		tpl.merge(context, strWriter);
		Log.debug("File %s%n%s",targetFile.getAbsolutePath(),strWriter.toString());
		return strWriter.toString();

	}

	public String interpolate(Map<String, Object> extraArgs,
			String templateString) {

		VelocityContext context = buildContext(extraArgs, true);

		VelocityEngine ve = getDefaultEngine();
		StringWriter strWriter = new StringWriter();
		ve.evaluate(context, strWriter, DigestUtils.md5Hex(templateString),
				templateString);
		Log.debug(strWriter.toString());
		return strWriter.toString();

	}

	public String interpolateJobConfig(Map<String, Object> extraArgs,
			File jobConfigFile) {
		VelocityContext context = buildContext(extraArgs, false);

		Template tpl = getTemplateForFile(jobConfigFile);
		StringWriter strWriter = new StringWriter();
		tpl.merge(context, strWriter);
		Log.debug("JOB Config %s%n%s",jobConfigFile.getAbsolutePath(),strWriter.toString());
		return strWriter.toString();
	}

	public String interpolateMarkerConfig(Map<String, Object> extraArgs,
			File markerFile) {
		VelocityContext context = buildContext(extraArgs, true);

		Template tpl = getTemplateForFile(markerFile);
		StringWriter strWriter = new StringWriter();
		tpl.merge(context, strWriter);
		Log.debug("Marker Config %s%n%s",markerFile.getAbsolutePath(),strWriter.toString());
		return strWriter.toString();
	}

	public String interpolateJobConfig(File jobConfigFile) {
		return interpolateJobConfig(null, jobConfigFile);
	}

	public String interpolateMarkerConfig(File markerFile) {
		return interpolateMarkerConfig(null, markerFile);
	}

	public String interpolate(File targetFile) {
		return this.interpolate(null, targetFile);

	}

	public String interpolate(String templateString) {
		return this.interpolate(null, templateString);

	}

	private VelocityEngine velocityEngine = null;

	private VelocityEngine getDefaultEngine() {
		if (velocityEngine == null) {
			velocityEngine = new VelocityEngine();
			velocityEngine.init();
		}

		return velocityEngine;
	}

	private VelocityContext buildContext(Map<String, Object> extraArgs,
			boolean addConfigFiles) {
		VelocityContext context = new VelocityContext();
		if (addConfigFiles) {
			configFiles(context);
		}
		context.put("runtimeArgs", Config.get.runtimeArgs());
		context.put("symlink-config-path", H.me.normalizePath(H.fsTools.getExternalDirectoryPath()));

		/*-
		 * System property 
		 * "java.home" Installation directory for Java Runtime Environment (JRE)
		 * "java.version" JRE version number 
		 * "os.name" Operating system name 
		 * "os.version" Operating system version 
		 * "user.dir" User working directory 
		 * "user.home" User home directory 
		 * "user.name" User account name
		 */
		context.put("java-home",H.me.normalizePath(System.getProperty("java.home")));
		context.put("java-version", System.getProperty("java.version"));
		context.put("os-name", System.getProperty("os.name"));
		context.put("os-version", System.getProperty("os.version"));
		context.put("user-dir", H.me.normalizePath(System.getProperty("user.dir")));
		context.put("user-home", H.me.normalizePath(System.getProperty("user.home")));
		context.put("user-name", System.getProperty("user.name"));

		/*-
		 * System env properties
		 */

		Map<String, String> env = System.getenv();
		for (String envName : env.keySet()) {
			context.put(envName.replace('.', '-'), env.get(envName));
		}

		/*-
		 * ExtraArgs properties
		 */
		if (extraArgs != null) {
			for (Entry<String, Object> exArg : extraArgs.entrySet()) {
				context.put(exArg.getKey().replace('.', '-'), exArg.getValue());
			}
		}
		return context;
	}

	private void configFiles(VelocityContext context) {
		context.put("job", Config.get.jobConfiguration());
		// context.put("usrSettings", Config.get.userSettings());
	}

	private Template getTemplateForFile(File templateFile) {
		String absolutePath = templateFile.getAbsolutePath();
		String filePath = FilenameUtils.getFullPathNoEndSeparator(absolutePath);
		VelocityEngine velocityEngine = veMap.get(filePath);
		if (velocityEngine == null) {
			velocityEngine = new VelocityEngine();
			Properties p = new Properties();
			p.setProperty("file.resource.loader.path", filePath);
			p.setProperty("runtime.log.logsystem.class",
					"org.apache.velocity.runtime.log.SystemLogChute");
			velocityEngine.init(p);
			veMap.put(filePath, velocityEngine);
		}

		return velocityEngine.getTemplate(FilenameUtils.getName(absolutePath));

	}

}
