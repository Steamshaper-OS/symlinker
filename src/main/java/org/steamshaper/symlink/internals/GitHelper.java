package org.steamshaper.symlink.internals;

import java.io.File;
import java.net.ProxySelector;
import java.net.URL;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.jgit.api.CloneCommand;
import org.eclipse.jgit.api.CreateBranchCommand;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.ListBranchCommand.ListMode;
import org.eclipse.jgit.api.PullResult;
import org.eclipse.jgit.api.Status;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.api.errors.InvalidRemoteException;
import org.eclipse.jgit.api.errors.TransportException;
import org.eclipse.jgit.lib.Constants;
import org.eclipse.jgit.lib.ProgressMonitor;
import org.eclipse.jgit.lib.Ref;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.storage.file.FileRepositoryBuilder;
import org.eclipse.jgit.transport.CredentialsProvider;
import org.eclipse.jgit.transport.FetchResult;
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;
import org.steamshaper.symlink.config.SymLinkCredential;
import org.steamshaper.symlink.config.SymLinkProjectConf;
import org.steamshaper.symlink.config.SymLinkProxyConf;
import org.steamshaper.symlink.output.ProjectSummary;
import org.steamshaper.symlink.utils.Config;
import org.steamshaper.symlink.utils.H;
import org.steamshaper.symlink.utils.Log;

public class GitHelper {

	private ConfigurableProxySelector proxySelector;
	private boolean useProxy;

	private static final String REMOTE_PREFIX = "refs/remotes/origin/";
	private static final String LOCAL_PREFIX = "refs/heads/";

	public GitHelper(Map<String, SymLinkProxyConf> proxyMap, boolean useProxy) {
		this.useProxy = useProxy;
		if (this.useProxy) {
			this.proxySelector = buildProxySelector(proxyMap);
			ProxySelector.setDefault(proxySelector);

		}
	}

	public GitHelper() {
		this(null, false);
	}

	public GitHelper(boolean useProxy) {
		this(null, useProxy);
	}

	private ConfigurableProxySelector buildProxySelector(Map<String, SymLinkProxyConf> proxyMap) {
		ConfigurableProxySelector configurableProxySelector = new ConfigurableProxySelector();
		addRoutes(proxyMap);
		return configurableProxySelector;
	}

	public void addRoutes(Map<String, SymLinkProxyConf> proxyMap) {
		if (useProxy && proxyMap != null && proxySelector != null) {
			proxySelector.addRoute(proxyMap);
		}
	}

	public void addDefault(SymLinkProxyConf defaultProxy) {
		proxySelector.addDefaultRoute(defaultProxy);

	}

	public ProjectSummary cloneRemote(SymLinkProjectConf prjConf, File workDirectory, boolean deleteIfExist)
			throws InvalidRemoteException, TransportException, GitAPIException {
		Git result = null;
		File localPath = new File(prjConf.getCheckoutFolder());
		try {
			if (localPath.canWrite()) {
				if (localPath.exists() && deleteIfExist) {
					localPath.delete();
				} else {

					return null;
				}

			}

			// then clone
			Log.info("Cloning from [%s] branch [%s] to [%s]", prjConf.getGitUrl(), prjConf.getGitBranch(),
					prjConf.getCheckoutFolder());

			CloneCommand gitCloneCmd = Git.cloneRepository();
			CredentialsProvider credProvider = getCredentialProvider(prjConf);
			if (credProvider != null) {
				gitCloneCmd.setCredentialsProvider(credProvider);
			}
			if (StringUtils.isNotBlank(prjConf.getGitBranch())) {
				gitCloneCmd.setBranch(prjConf.getGitBranch());
			}

			gitCloneCmd.setURI(prjConf.getGitUrl()).setDirectory(localPath);
			gitCloneCmd.setProgressMonitor(new SymLinkProgressMonitor());
			result = gitCloneCmd.call();
			// Note: the call() returns an opened repository already which needs
			// to be closed to avoid file handle leaks!
			Repository repository = result.getRepository();
			Log.info("Having repository: [%s]", repository.getDirectory());
			ProjectSummary prjSummary = genSummary(prjConf, repository);
			return prjSummary;
		} finally {
			if (result != null) {
				result.close();
			}

		}
	}

	private CredentialsProvider getCredentialProvider(SymLinkProjectConf prjConf) {
		UsernamePasswordCredentialsProvider upCp = null;
		if (prjConf.getAuth() != null) {
			upCp = new UsernamePasswordCredentialsProvider(prjConf.getAuth().getUsername(),
					prjConf.getAuth().getPassword());
		} else {
			try {
				URL gitUrl = new URL(prjConf.getGitUrl());
				String gitHost = gitUrl.getHost();
				Map<String, SymLinkCredential> credentialMap = Config.get.userSettings().getCredentialMap();
				if (credentialMap != null) {
					for (Entry<String, SymLinkCredential> credential : credentialMap.entrySet()) {
						if (credential.getKey().contains(gitHost)) {
							upCp = new UsernamePasswordCredentialsProvider(credential.getValue().getUsername(),
									credential.getValue().getPassword());
							break;
						}

					}
				}
			} catch (Exception e) {
				Log.debug("Error @%s exception%n%s", H.me.getCurrentMethodInfo(), e);
				// DO NOTHING
			}

		}
		return upCp;
	}

	public ProjectSummary updateRemote(SymLinkProjectConf prjConf, File workDirectory) {
		FileRepositoryBuilder builder = new FileRepositoryBuilder();
		File localPath = new File(prjConf.getCheckoutFolder() + "/.git");
		Log.debug("Updating [%s]", prjConf.getProjectName());
		Repository repository = null;
		try {
			repository = builder.setGitDir(localPath).readEnvironment() // scan
																		// environment
																		// GIT_*
																		// variables
					.findGitDir() // scan up the file system tree
					.build();
			Log.debug("Fetching messages on [%s] ...", prjConf.getProjectName());
			Git gitRepo = new Git(repository);
			FetchResult fetchResoults = gitRepo.fetch().setCheckFetchedObjects(true)
					.setCredentialsProvider(getCredentialProvider(prjConf)).setProgressMonitor(new SymLinkProgressMonitor()).call();

			Log.debug("Fetch on %s DONE", prjConf.getProjectName());
			Log.debug("Fetch results [%s]", fetchResoults.getMessages());
			Status status = printRepoStatus(gitRepo);

			String checkOutIfExist = Config.get.runtimeArgs().getCheckOutIfExist();
			String fullBranch = gitRepo.getRepository().getFullBranch();
			Log.debug("Current branch %s", fullBranch);

			if (StringUtils.isNotBlank(checkOutIfExist)) {

				checkOutIfExist = checkOutIfExist.trim();

				if ((LOCAL_PREFIX + checkOutIfExist).equals(fullBranch)) {
					Log.debug("This repository is already on %s", checkOutIfExist);
				} else {

					for (Ref ref : gitRepo.branchList().setListMode(ListMode.ALL).call()) {
						String branchFulName = ref.getName();
						Log.debug("Found branch : %s", branchFulName);
						if (branchFulName.equals(LOCAL_PREFIX + checkOutIfExist)) {
							gitRepo.checkout().setName(checkOutIfExist).call();
							Log.info("Checkout from local : %s", branchFulName);
							break;
						} else if (branchFulName.equals(REMOTE_PREFIX + checkOutIfExist)) {
							gitRepo.checkout().setName(checkOutIfExist)
									.setUpstreamMode(CreateBranchCommand.SetupUpstreamMode.TRACK)
									.setCreateBranch(Boolean.TRUE).setStartPoint("origin/" + checkOutIfExist).call();
							Log.info("Checkout from remote : %s", branchFulName);
							break;
						}
					}
				}

			}

			if (Config.get.runtimeArgs().isPullCommits() || prjConf.isPullCommits()) {
				if (status.getConflictingStageState().isEmpty()) {

					try {
						Log.debug("Pulling on [%s] ... ", prjConf.getProjectName());
						PullResult pullResult = gitRepo.pull().setCredentialsProvider(getCredentialProvider(prjConf))
								.setTimeout(10000).call();

						if (pullResult.isSuccessful()) {
							Log.debug("Pull on [%s] DONE!", prjConf.getProjectName());
							if (Config.get.runtimeArgs().isVerbose()) {
								Log.info("Pull results for [%s]", prjConf.getProjectName(),
										pullResult.getMergeResult().toString());
							}
						} else {
							Log.error("Error on pull! %n%s", pullResult.getMergeResult().toString());
						}
					} catch (GitAPIException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				} else {
					Log.error("Some file is conflicting [%s] PULL ABORTED!", status.getConflicting());
				}
			}

			// Log.printEmptyLine();

			// Log.info("Cleaning repos ...");
			// Properties ret = gitRepo.gc().call();
			// for(Map.Entry<Object, Object> entry : ret.entrySet()) {
			// Log.info("Ret: " + entry.getKey() + ": " + entry.getValue());
			// }

			ProjectSummary prjSummary = genSummary(prjConf, repository);
			return prjSummary;
		} catch (Exception e) {

			Log.error("FETCH ERROR %s", e.getMessage());

			e.printStackTrace();
		} finally {
			try {
				repository.close();
			} catch (Throwable t) {

			}
		}
		return null;

	}

	private ProjectSummary genSummary(SymLinkProjectConf prjConf, Repository repository) {
		ProjectSummary prjSummary = new ProjectSummary();
		prjSummary.setName(prjConf.getProjectName());
		prjSummary.setRepo(prjConf.getGitUrl());
		try {
			prjSummary.setBranch(repository.getFullBranch());
		} catch (Throwable t) {
			prjSummary.setBranch("!#error");
			Log.error("Unable to set branch cause: %s ", t.getMessage());
		}
		try {
			prjSummary.setCommit(repository.resolve(Constants.HEAD).getName());
		} catch (Throwable t) {
			prjSummary.setCommit("!#error");
			Log.error("Unable to set commit cause: %s ", t.getMessage());
		}
		return prjSummary;
	}

	private Status printRepoStatus(Git gitRepo) throws GitAPIException {
		Status status = gitRepo.status().call();
		if (Config.get.runtimeArgs().isVerbose()) {
			Log.info("Added: " + status.getAdded());
			Log.info("Changed: " + status.getChanged());
			Log.info("Conflicting: " + status.getConflicting());
			Log.info("ConflictingStageState: " + status.getConflictingStageState());
			Log.info("IgnoredNotInIndex: " + status.getIgnoredNotInIndex());
			Log.info("Missing: " + status.getMissing());
			Log.info("Modified: " + status.getModified());
			Log.info("Removed: " + status.getRemoved());
			Log.info("Untracked: " + status.getUntracked());
			Log.info("UntrackedFolders: " + status.getUntrackedFolders());
		}
		return status;
	}

}
