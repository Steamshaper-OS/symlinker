package org.steamshaper.symlink.internals;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.NotImplementedException;
import org.steamshaper.symlink.utils.Log;

public class LinkBuilder {

	public void createSymbolicLink(File pathSource, File pathLink) {

		if (!pathSource.exists()) {
			Log.error("Resource [%s] NOT FOUND!",pathSource.getAbsolutePath());
		} else {
			Path target = pathSource.toPath();

			Path newLink = pathLink.toPath();
			try {
				if (!pathLink.getParentFile().exists()) {
					FileUtils.forceMkdir(pathLink.getParentFile());
				}
				Files.createSymbolicLink(newLink, target);
				Log.info("[%s] --> [%s]", target,newLink);
			} catch (IOException | UnsupportedOperationException x) {
				throw new NotImplementedException(String.format("File already exists source [%s] --> target [%s]",target,newLink),x);
			}
		}
	}

	public void createSymbolicLink(String pathSource, String pathLink) {
		File pathSourceFile = new File(pathSource);
		File pathLinkFile = new File(pathLink);
		this.createSymbolicLink(pathSourceFile, pathLinkFile);
	}

}
