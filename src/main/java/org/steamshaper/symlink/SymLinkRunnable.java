package org.steamshaper.symlink;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.NotImplementedException;
import org.apache.commons.lang3.StringUtils;
import org.eclipse.jgit.api.errors.TransportException;
import org.steamshaper.symlink.config.SymLinkConfig;
import org.steamshaper.symlink.config.SymLinkMarker;
import org.steamshaper.symlink.config.SymLinkProfileConfig;
import org.steamshaper.symlink.config.SymLinkProjectConf;
import org.steamshaper.symlink.internals.ConfigScanner;
import org.steamshaper.symlink.internals.GitHelper;
import org.steamshaper.symlink.internals.LinkBuilder;
import org.steamshaper.symlink.internals.RunContext;
import org.steamshaper.symlink.output.ProjectSummary;
import org.steamshaper.symlink.output.WorkspaceSummary;
import org.steamshaper.symlink.utils.Config;
import org.steamshaper.symlink.utils.H;
import org.steamshaper.symlink.utils.JSON;
import org.steamshaper.symlink.utils.Log;

public class SymLinkRunnable implements Runnable {

	private List<String> activeProfileName = new ArrayList<>();

	public SymLinkRunnable(List<String> activeProfile) {
		if (activeProfile != null) {
			this.activeProfileName.addAll(activeProfile);
		}
		if (activeProfileName.size() == 0) {
			activeProfileName.add(Constants.DEFAULT_PROFILE_NAME);
		} // TODO Auto-generated constructor stub
	}

	@Override
	public void run() {

		Log.wrappedinfo("Updating projects ...");
		Map<String, RunContext> runContextMap = new HashMap<String, RunContext>();
		if (Config.get.jobConfiguration().getProfiles() == null) {
			Log.error("No profile defined in configuration file!");
		} else {
			GitHelper gitH = new GitHelper(Config.get.runtimeArgs().isUseProxy());
			if (Config.get.runtimeArgs().isUseProxy()) {
				if (Config.get.jobConfiguration().getDefaultProxy() != null) {
					gitH.addDefault(Config.get.jobConfiguration().getDefaultProxy());
				} else if (Config.get.userSettings().getDefaultProxy() != null) {
					gitH.addDefault(Config.get.userSettings().getDefaultProxy());
				}
			}

			for (Entry<String, SymLinkProfileConfig> profile : Config.get.jobConfiguration().getProfiles().entrySet()) {
				if (activeProfileName.contains(profile.getKey())) {

					if (Config.get.runtimeArgs().isUseProxy()) {
						gitH.addRoutes(profile.getValue().getProxyMap());
					}

					if (!doGitCheckout(Config.get.jobConfiguration(), profile.getKey(), profile.getValue(), gitH)) {

						break;
					}

					runContextMap.put(profile.getKey(),
							RunContext.build(Config.get.jobConfiguration(), profile.getValue(), profile.getKey()));
				}

			}

			if (runContextMap.size() == 0) {
				Log.warning("No profile matching configuration!");
			}

			for (RunContext currentRunnning : runContextMap.values()) {

				if (!Config.get.runtimeArgs().isNoLink() && !doLink(currentRunnning)) {
					break;
				}
			}
		}
	}

	private boolean doGitCheckout(SymLinkConfig config, String key, SymLinkProfileConfig profileConfig,
			GitHelper gitH) {

		WorkspaceSummary wsSummary = new WorkspaceSummary();
		List<ProjectSummary> projectSummary = wsSummary.getProject();

		for (Entry<String, SymLinkProjectConf> project : profileConfig.getProjects().entrySet()) {
			String projectId = project.getKey();
			Log.printEmptyLine();
			Log.info("%s ...", projectId);

			SymLinkProjectConf prjConf = project.getValue();

			File chOutForlder = null;

			String baseCtxPath = StringUtils.isNotBlank(config.getWorkDirectory()) ? config.getWorkDirectory() : "";

			if (StringUtils.isNotBlank(prjConf.getCheckoutFolder())) {

				chOutForlder = new File(FilenameUtils.concat(baseCtxPath, prjConf.getCheckoutFolder()));
				Log.debug("Checkout %s --> [%s]", projectId, chOutForlder.getAbsolutePath());
			} else {
				chOutForlder = new File(FilenameUtils.concat(baseCtxPath, projectId));
				Log.error("Project %s don't have a checkout folder setting to %s -->", projectId);
			}

			ProjectSummary summary = null;
			try {
				if (chOutForlder.exists()) {
					// FIXME check if is the same
					Log.toDo("check if is the same repository @%s", H.me.getCurrentMethodInfo());
					// if(!gitH.isAGitRepository(chOutForlder))

					summary = gitH.updateRemote(prjConf, H.me.getWorkDirectory(config));
				} else {
					summary = gitH.cloneRemote(prjConf, H.me.getWorkDirectory(config), false);

				}
			} catch (Exception tEx) {
				Log.error("Unable to connect to remote repository, ERROR: %s %n",tEx.getMessage());
			}
			if (summary != null) {
				projectSummary.add(summary);
				Log.info("%s --> %s [OK]", projectId, summary.getBranch());
			}

			// Log.printEmptyLine();

		}

		if (Config.get.runtimeArgs().getWriteSummary()) {
			try {
				File file = new File("smlink-summary.json");
				if (file.exists()) {
					file.delete();
				}
				FileUtils.write(file, JSON.help.getMapper().writeValueAsString(wsSummary), "UTF-8");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return true;
	}

	private boolean doLink(RunContext ctx) {
		/*-
		 * EXECUTION PLAN
		 * 
		 * 1.Find Markers for profile
		 * 
		 * 2.Check if destination base folder exist
		 * 
		 * 3.For each marker
		 * 		1.	Check if the link already exist 
		 * 		 	if TRUE check if have the same origin
		 * 			if FALSE check overwrite properties in marker file or job config
		 */
		Log.wrappedinfo("Running profile [%s]", ctx.getProfileName());

		List<SymLinkMarker> markers = ConfigScanner.findMarkers(ctx);

		if (markers != null && !markers.isEmpty()) {
			File dstBaseFolder = getDestinationBaseFolder(ctx);
			if (!dstBaseFolder.exists()) {
				try {
					FileUtils.forceMkdir(dstBaseFolder);
				} catch (IOException e) {
					// FIXME
					throw new NotImplementedException(
							"Destination dir create ERROR HANDLING com.hp.symlink.SymLinker.executeProfile(RunContext)");
				}
			}

			int linkCounter = 0;
			int alreadyLinked = 0;
			for (SymLinkMarker marker : markers) {
				Log.debug("Marker file [%s], %n %s", H.me.removeHomeDir(marker.getMarkerFile().getPath()), marker);

				File srcFile = calcLnkSrc(marker, ctx);

				File destFile = calcLnkDest(dstBaseFolder, marker, ctx);
				Log.debug("Link %s --> %s", srcFile, destFile);

				Path destPath = destFile.toPath();
				boolean isDestSymLink = destFile.exists() && Files.isSymbolicLink(destPath);

				if (isDestSymLink) {

					try {
						Path linkDest = Files.readSymbolicLink(destPath);
						if (linkDest.equals(srcFile.toPath())) {
							// is the same symlink no relink necessary
							alreadyLinked++;
						} else {
							// The destination file exist and have a different
							// source
							if (marker.getOverwriteIfExist() || ctx.getJobConfiguration().getOverwriteIfExist()
									|| Config.get.runtimeArgs().getOverwriteIfExist()) {
								Log.info("Link [%s] will be replaced from marker %s!", destFile.getAbsolutePath(),
										marker.getMarkerFile().getPath());
								destFile.delete();
								createLink(srcFile, destFile);
								linkCounter++;
							}
						}
					} catch (IOException e) {
						e.printStackTrace();
						throw new NotImplementedException(
								"Error handling on Files.readSymbolicLink " + H.me.getCurrentMethodInfo());
					}
				} else {
					createLink(srcFile, destFile);
					linkCounter++;
				}

			}
			Log.info("Link summary (%s) new, (%s) already linked!", linkCounter, alreadyLinked);
		} else {
			Log.info("No marker was found in [%s]", StringUtils.join(ctx.getScanPaths(), ","));
		}
		return true;
	}

	private void createLink(File srcFile, File destFile) {
		LinkBuilder lb = new LinkBuilder();
		lb.createSymbolicLink(srcFile, destFile);
	}

	private File getDestinationBaseFolder(RunContext ctx) {
		String output = StringUtils.isNotBlank(ctx.getJobConfiguration().getWorkDirectory())
				? ctx.getJobConfiguration().getWorkDirectory() : "";

		if (StringUtils.isNotBlank(ctx.getCurrentProfile().getDestinationPath())) {
			output = FilenameUtils.concat(output, ctx.getCurrentProfile().getDestinationPath());
		} else if (StringUtils.isNotBlank(ctx.getJobConfiguration().getTarget())) {
			output = FilenameUtils.concat(output, ctx.getJobConfiguration().getTarget());
		}
		if (StringUtils.isBlank(output)) {
			output = Constants.getDefaultDestDir();
		}

		return new File(output);
	}

	private File calcLnkDest(File destinationBase, SymLinkMarker marker, RunContext ctx) {
		String path = destinationBase.getAbsolutePath();
		if (StringUtils.isNotBlank(marker.getDestinationPath())) {
			path = FilenameUtils.concat(path, marker.getDestinationPath());
		}
		if (StringUtils.isNotBlank(marker.getRename())) {
			path = FilenameUtils.concat(path, marker.getRename());
		} else {
			path = FilenameUtils.concat(path, marker.getMarkerFile().getParentFile().getName());
		}

		return new File(path);
	}

	private File calcLnkSrc(SymLinkMarker marker, RunContext ctx) {
		return marker.getMarkerFile().getParentFile();
	}

}
