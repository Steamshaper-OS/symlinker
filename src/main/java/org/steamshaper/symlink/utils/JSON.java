package org.steamshaper.symlink.utils;

import static com.fasterxml.jackson.databind.DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.NotImplementedException;
import org.steamshaper.symlink.Constants;
import org.steamshaper.symlink.Constants.GENERATE_DEFAULT;
import org.steamshaper.symlink.config.SymLinkConfig;
import org.steamshaper.symlink.config.SymLinkMarker;
import org.steamshaper.symlink.config.external.SymLinkExternalSettings;
import org.steamshaper.symlink.internals.VariableHelp;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonParser.Feature;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.module.jsonSchema.JsonSchema;
import com.fasterxml.jackson.module.jsonSchema.factories.SchemaFactoryWrapper;

public class JSON {

	private ObjectMapper mapper = new ObjectMapper();
	public final static JSON help = new JSON();

	private JSON() {
		mapper.configure(FAIL_ON_UNKNOWN_PROPERTIES, false);
		mapper.configure(SerializationFeature.INDENT_OUTPUT, true);
		mapper.configure(Feature.ALLOW_COMMENTS, true);
	}

	public ObjectMapper getMapper() {
		return mapper;
	}

	public SymLinkConfig readJobConfig(File jobConfigFile) {
		try {
			return getMapper().readValue(VariableHelp.me.interpolateJobConfig(jobConfigFile), SymLinkConfig.class);
		} catch (IOException e) {
			throw new NotImplementedException(
					"com.hp.symlink.utils.JSON.readJobConfig(File)");
		}
	}

	public SymLinkMarker readMarker(File markerFile) {
		try {
			
			SymLinkMarker readValue = getMapper().readValue(VariableHelp.me.interpolateMarkerConfig(markerFile),
					SymLinkMarker.class);
			readValue.setMarkerFile(markerFile);
			return readValue;
		} catch (IOException e) {
			throw new NotImplementedException(
					"com.hp.symlink.utils.JSON.readMarker(File)");
		}
	}

	public File writeConfig(SymLinkConfig toWrite, String basePath,
			String profile, boolean forceOverwrite)
			throws JsonGenerationException, JsonMappingException, IOException {
		File out = new File(FilenameUtils.concat(basePath,
				Constants.getJobConfigFileName()));
		if (!forceOverwrite && out.exists()) {
			Log.error("File already exist use overwrite = true");
			return out;
		} else {
			if (out.exists()) {
				out.delete();
			}

			getMapper().writeValue(out, toWrite);
			return out;
		}

	}

	public File writeConfig(SymLinkMarker toWrite, String basePath,
			String profile, boolean forceOverwrite)
			throws JsonGenerationException, JsonMappingException, IOException {
		File out = new File(FilenameUtils.concat(basePath,
				Constants.getMarkerFileName(profile)));
		if (!forceOverwrite && out.exists()) {
			Log.error("File already exist use overwrite = true");
			return out;
		} else {
			if (out.exists()) {
				out.delete();
			}
			Log.info("Writing marker [%s]",out.getAbsolutePath());
			getMapper().writeValue(out, toWrite);
			return out;
		}

	}

	public <T> String generateSchema(Class<T> targetClass) {
		String output = "";
		try {
			SchemaFactoryWrapper visitor = new SchemaFactoryWrapper();
			getMapper().acceptJsonFormatVisitor(
					getMapper().constructType(targetClass), visitor);
			JsonSchema jsonSchema = visitor.finalSchema();
			output = getMapper().writerWithDefaultPrettyPrinter()
					.writeValueAsString(jsonSchema);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return output;
	}

	public SymLinkExternalSettings readExternalConfig(File conf,
			GENERATE_DEFAULT generate) {
		try {
			if (!conf.exists()
					&& GENERATE_DEFAULT.IF_NOT_EXISTS.equals(generate)) {
				SymLinkExternalSettings defaultSettings = generateExternalSettingsDefault();
				getMapper().writeValue(conf, defaultSettings);
			}
			
			
			
			return getMapper().readValue(VariableHelp.me.interpolate(conf), SymLinkExternalSettings.class);

		} catch (IOException e) {
			throw new NotImplementedException(
					"com.hp.symlink.utils.JSON.readJobConfig(File)");
		}

	}

	private SymLinkExternalSettings generateExternalSettingsDefault() {
		return new SymLinkExternalSettings();
	}
}
