package org.steamshaper.symlink.utils;

import static org.fusesource.jansi.Ansi.ansi;

import org.fusesource.jansi.Ansi.Color;
import org.fusesource.jansi.Ansi.Erase;
import org.steamshaper.symlink.Constants;
import org.fusesource.jansi.AnsiConsole;

public class Log {
	private static final String INFO = " "; // "> INFO > ";
	private static Verbosity verbosity = Verbosity.INFO;

	static {
		AnsiConsole.systemInstall();
		System.out.println(ansi().reset().cursor(0, 0).eraseScreen(Erase.ALL));
	}

	public enum Verbosity {
		DEBUG(5), WARNING(4), INFO(3), ERROR(2);

		private int value;

		Verbosity(int value) {
			this.value = value;
		}

		public boolean shouldLog(Verbosity requested) {
			return requested.value <= this.value;
		}
	}

	private static final String DEFAULT_SEPARATOR = ".";

	public static void debug(String format, Object... values) {
		if (verbosity.shouldLog(Verbosity.DEBUG)) {
			System.out.println(ansi().fg(Color.BLUE).bold().a("> DEBUG > ")
					.boldOff().fg(Color.RED).format(format, values).reset());
		}
	}

	public static void info(String format, Object... values) {
		if (verbosity.shouldLog(Verbosity.INFO)) {
			System.out.println(ansi().fg(Color.DEFAULT).bold().a(INFO)
					.boldOff().format(format, values).reset());
		}
	}

	public static void error(String format, Object... values) {
		if (verbosity.shouldLog(Verbosity.ERROR)) {
			System.err.println(ansi().fg(Color.RED).bold().a("> ERROR > ")
					.fg(Color.DEFAULT).bg(Color.DEFAULT)
					.a(String.format(format, values)).reset());
		}
	}

	public static void warning(String format, Object... values) {
		if (verbosity.shouldLog(Verbosity.WARNING)) {
			System.err.println(ansi().fg(Color.RED).bold().a("> WARN > ")
					.fg(Color.YELLOW).bg(Color.DEFAULT).format(format, values)
					.reset());
		}
	}

	public static void wrappedinfo(String format, Object... values) {
		if (verbosity.shouldLog(Verbosity.INFO)) {
			String formattedString = String.format(format, values);
			printLine(formattedString.length()*2);
			info(formattedString);
			printLine(formattedString.length()*2);
		}
	}

	public static void wrappedDebug(String format, Object... values) {
		if (verbosity.shouldLog(Verbosity.DEBUG)) {
			String formattedString = String.format(format, values);
			printLine(formattedString.length()*2);
			debug(formattedString);
			printLine(formattedString.length()*2);
		}
	}

	public static void printLine(int length) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < Math.min(length, 120); i++) {
			sb.append(DEFAULT_SEPARATOR);
		}

		System.out.println(ansi().fg(Color.CYAN).a(sb.toString()).reset());
	}

	
	public static void printEmptyLine() {
		System.out.println("");
	}

	
	public static void logLogo() {
		for (String logoRow : Constants.LOGO) {
			System.out.println(ansi().bold().fg(Color.DEFAULT).a(logoRow)
					.reset());
		}
		Log.info("Version: %s", Constants.getVersion().toString());
	}

	public static void setVerbosity(Verbosity reqVerbosity) {
		verbosity = reqVerbosity;
	}

	public static void toDo(String format, Object... values) {
		Log.wrappedDebug(format, values);

	}

	public static void toBeRemoved() {
		Log.wrappedDebug("To be removed @", H.me.getCurrentMethodInfo(1));

	}

	public static void infoInline(String format, Object... values) {
		if (verbosity.shouldLog(Verbosity.INFO)) {
			System.out.print(ansi().fg(Color.DEFAULT).bold().a(INFO)
					.boldOff().format(format, values).reset());
		}
		
	}

}
