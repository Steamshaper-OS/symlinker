package org.steamshaper.symlink.utils;

import java.io.File;

import org.apache.commons.lang3.StringUtils;
import org.steamshaper.symlink.CliArgs;
import org.steamshaper.symlink.Constants;
import org.steamshaper.symlink.config.SymLinkConfig;
import org.steamshaper.symlink.config.external.SymLinkExternalSettings;
import org.steamshaper.symlink.internals.ConfigScanner;
import org.steamshaper.symlink.internals.MissingConfigurationException;

import com.beust.jcommander.JCommander;

public class Config {

	private SymLinkExternalSettings userSettings;

	private SymLinkConfig config;

	private CliArgs cliArgs;

	private JCommander jc;

	public final static Config get = new Config();

	private Config() {

	}

	public static CliArgs init(String[] args) {
		get.cliArgs = new CliArgs();
		get.jc = null;
		try {
			get.jc = new JCommander(get.cliArgs, args);
		} catch (Exception ex) {
			get.jc.usage();
		}

		return get.cliArgs;
	}

	public static void printUsage() {
		get.jc.usage();

	}

	public SymLinkConfig jobConfiguration() {

		if (config == null) {
			try {
				config = initJobConfig();
			} catch (MissingConfigurationException e) {
				Log.error(e.getMessage());
				System.exit(0);
			}
		}
		return config;
	}

	private SymLinkConfig initJobConfig() throws MissingConfigurationException {
		// Log.wrappedDebug("User directory [%s]",
		// System.getProperty("user.dir"));
		File jobFile = null;
		if (StringUtils.isNoneBlank(cliArgs.getJobFilePath())) {
			jobFile = new File(cliArgs.getJobFilePath());
			if (!jobFile.exists() || jobFile.canRead()) {
				if (!cliArgs.isCreateJobConfigFile()) {
					Log.error("The file [%s] don't exist or isn't readable!",
							cliArgs.getJobFilePath());
					return null;
				} else {
					Log.info("A default file will be created!");
				}
			}
		}

		return ConfigScanner
				.readJobConfig(
						jobFile,
						cliArgs.isCreateJobConfigFile() ? Constants.GENERATE_DEFAULT.IF_NOT_EXISTS
								: Constants.GENERATE_DEFAULT.NO);
	}

	public SymLinkExternalSettings userSettings() {
		if (userSettings == null) {
			userSettings = ConfigScanner
					.readUserSettings(Constants.GENERATE_DEFAULT.IF_NOT_EXISTS);
		}
		return userSettings;
	}

	public CliArgs runtimeArgs() {
		return cliArgs;
	}
}