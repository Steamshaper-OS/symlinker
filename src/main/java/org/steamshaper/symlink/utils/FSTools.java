package org.steamshaper.symlink.utils;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;

public class FSTools {

	private static final String SYMLINK_ENV_VAR = "SYMLINK_CONF";

	private static final String SYMLINK_JVM_ARG = "symlink.home";

	private static final String DEFAULT_HOME_DIRECTORY_PATH = FilenameUtils
			.concat(System.getProperty("user.home"), "_symlink_home");

	private String homeDirectoryPath = DEFAULT_HOME_DIRECTORY_PATH;

	FSTools() {
		String home_location = System.getProperty(SYMLINK_JVM_ARG);
		if (home_location == null) {
			home_location = System.getenv(SYMLINK_ENV_VAR);
		}
		try {
			if (home_location != null) {
				File homeDir = new File(home_location);
				if (!homeDir.exists()) {
					FileUtils.forceMkdir(homeDir);
				}
				homeDirectoryPath = home_location;

			} else {
				getExternalDirectory();
			}
			Log.info("Home directory: [%s]",homeDirectoryPath);
		} catch (IOException e) {
			throw new IllegalAccessError("Unable to find home directory");
		}
	}

	public File getExternalDirectory() throws IOException {
		File homeDir = new File(homeDirectoryPath);
		if (!homeDir.exists()) {
			FileUtils.forceMkdir(homeDir);
		}
		return homeDir;
	}

	public String getExternalDirectoryPath() {
		return homeDirectoryPath;
	}

	public byte[] readResourceAsBytes(String resourceName, String... pathTokens) {

		File targetResource = getResource(resourceName, pathTokens);
		byte[] output = null;
		if (targetResource.exists()) {
			try {
				output = FileUtils.readFileToByteArray(targetResource);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return output;
	}

	public File getResource(String resourceName, String... pathTokens) {
		String basePath = appendToHomePath(pathTokens);
		basePath = FilenameUtils.concat(basePath, resourceName);
		File targetResource = new File(basePath);
		return targetResource;
	}

	public String appendToHomePath(String... pathTokens) {
		String basePath = homeDirectoryPath;
		for (String token : pathTokens) {
			basePath = FilenameUtils.concat(basePath,
					removeTailAndHeadSeparator(token));
		}
		return basePath;
	}

	private String removeTailAndHeadSeparator(String replace) {
		String out = FilenameUtils.normalizeNoEndSeparator(replace, true);

		if (out != null && out.length() > 1 && out.charAt(0) == '/') {
			out = out.substring(1);
		}

		return out;
	}

	public File createResource(String name, String... pathTokens)
			throws IOException {
		String resourcePath = appendToHomePath(pathTokens);
		File baseDir = new File(resourcePath);
		if (!baseDir.exists()) {
			FileUtils.forceMkdir(baseDir);
		}

		return new File(FilenameUtils.concat(resourcePath, name));
	}

}
