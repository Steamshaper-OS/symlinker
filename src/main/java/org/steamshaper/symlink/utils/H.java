package org.steamshaper.symlink.utils;

import java.io.File;

import org.apache.commons.io.FilenameUtils;
import org.steamshaper.symlink.Constants;
import org.steamshaper.symlink.config.SymLinkConfig;

public class H {

	public static final H me = new H();

	private H() {

	}

	public static final FSTools fsTools = new FSTools();

	/**
	 * Get the method name for a depth in call stack. <br />
	 * Utility function
	 * 
	 * @param depth
	 *            depth in the call stack (0 means current method, 1 means call
	 *            method, ...)
	 * @return method name
	 */
	public String getCurrentMethodInfo() {
		final StackTraceElement[] ste = Thread.currentThread().getStackTrace();

		// System.
		// out.println(ste[ste.length-depth].getClassName()+"#"+ste[ste.length-depth].getMethodName());
		// return ste[ste.length - depth].getMethodName(); //Wrong, fails for
		// depth = 0

		StackTraceElement stackTraceElement = null;
		for (int i = 0; i < ste.length; i++) {
			stackTraceElement = ste[i];
			if (this.getClass().getName()
					.equals(stackTraceElement.getClassName())
					&& "getCurrentMethodInfo".contains(stackTraceElement
							.getMethodName())) {
				return ste[i + 1].getClassName() + "."
						+ ste[i + 1].getMethodName() + "@Line:"
						+ ste[i + 1].getLineNumber();
			}
		}

		return "";
	}

	public String getCurrentMethodInfo(int offset) {
		final StackTraceElement[] ste = Thread.currentThread().getStackTrace();

		// System.
		// out.println(ste[ste.length-depth].getClassName()+"#"+ste[ste.length-depth].getMethodName());
		// return ste[ste.length - depth].getMethodName(); //Wrong, fails for
		// depth = 0

		StackTraceElement stackTraceElement = null;
		for (int i = 0; i < ste.length; i++) {
			stackTraceElement = ste[i];
			if (this.getClass().getName()
					.equals(stackTraceElement.getClassName())
					&& "getCurrentMethodInfo".contains(stackTraceElement
							.getMethodName())) {
				return ste[i + 1 + offset].getClassName() + "."
						+ ste[i + 1 + offset].getMethodName() + "@Line:"
						+ ste[i + 1 + offset].getLineNumber();
			}
		}

		return "";
	}

	public File getWorkDirectory(SymLinkConfig config) {
		if (config.getWorkDirectory() != null) {
			return new File(config.getWorkDirectory());
		} else {
			return new File(Constants.getHomeDirectory());
		}
	}

	public String normalizePath(String property) {
		return FilenameUtils.normalize(property, true);
	}

	public Object removeHomeDir(String absolutePath) {
		String output = FilenameUtils.normalize(absolutePath, true);
		output = output.replace(Constants.getHomeDirectory(), "");
		return output;
	}
}
