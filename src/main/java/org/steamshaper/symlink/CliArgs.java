package org.steamshaper.symlink;

import java.util.List;

import com.beust.jcommander.Parameter;

public class CliArgs {

	@Parameter(names = { "--mode", "-m" }, description = "Select the run mode")
	private String mode;

	@Parameter(names = { "--profile", "-p" }, description = "Run profile")
	private List<String> profiles;

	@Parameter(names = { "--debug", "-d" }, description = "Debug mode")
	private boolean debug = false;

	@Parameter(names = { "--help", "-h" }, description = "Print help", help = true)
	private boolean help = false;

	@Parameter(names = { "--create-default" }, description = "Create default config file")
	private boolean createJobConfigFile = false;

	@Parameter(names = { "--job-descriptor", "-j" }, description = "Path to job file descriptor")
	private String jobFilePath = null;

	@Parameter(names = { "--generate-schemas" }, description = "Generate JSON Schema for config Files")
	private boolean generateJsonSchema = false;

	@Parameter(names = { "--use-proxy" }, description = "Chose if use proxy", arity = 1)
	private boolean useProxy = true;

	@Parameter(names = { "--generate-marker" }, description = "Generate marker file on current folder")
	private boolean generateMarker;

	@Parameter(names = { "--pull-new-commits", "-n" }, description = "Pull new commits")
	private boolean pullCommits;

	@Parameter(names = { "--verbose", "-v" }, description = "Verbose mode")
	private boolean verbose =  false;

	@Parameter(names = { "--skip-link-phase", "-s" }, description = "Pull new commits")
	private boolean noLink =  false;

	@Parameter(names = { "--checkout-if-exists", "-ck" }, description = "Checkout branch if exist on remote")
	private String checkOutIfExist;

	@Parameter(names = { "--overwrite-if-exists", "-o" }, description = "Overwrite symbolic link" )
	private boolean overwriteIfExist = false;

	@Parameter(names = { "--write-summary", "-S" }, description = "Write project summary" )
	private boolean writeSummary =  true;

	
	
	
	
	
	
	public boolean isGenerateJsonSchema() {
		return generateJsonSchema;
	}

	public void setGenerateJsonSchema(boolean generateJsonSchema) {
		this.generateJsonSchema = generateJsonSchema;
	}

	public String getMode() {
		return mode;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}

	public List<String> getProfiles() {
		return profiles;
	}

	public void setProfiles(List<String> profiles) {
		this.profiles = profiles;
	}

	public boolean isDebug() {
		return debug;
	}

	public void setDebug(boolean debug) {
		this.debug = debug;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CliArgs [");
		if (mode != null)
			builder.append("mode=").append(mode).append(", ");
		if (profiles != null)
			builder.append("profiles=").append(profiles).append(", ");
		builder.append("debug=").append(debug).append("]");
		return builder.toString();
	}

	public boolean isHelp() {
		return help;
	}

	public void setHelp(boolean help) {
		this.help = help;
	}

	public boolean isCreateJobConfigFile() {
		return createJobConfigFile;
	}

	public void setCreateJobConfigFile(boolean createJobConfigFile) {
		this.createJobConfigFile = createJobConfigFile;
	}

	public String getJobFilePath() {
		return jobFilePath;
	}

	public void setJobFilePath(String jobFilePath) {
		this.jobFilePath = jobFilePath;
	}

	public boolean isUseProxy() {
		return useProxy;
	}

	public void setUseProxy(boolean useProxy) {
		this.useProxy = useProxy;
	}

	public boolean isGenerateMarker() {
		return generateMarker;
	}

	public void setGenerateMarker(boolean generateMarker) {
		this.generateMarker = generateMarker;
	}

	public boolean isPullCommits() {
		return pullCommits;
	}

	public void setPullCommits(boolean pullCommits) {
		this.pullCommits = pullCommits;
	}

	public boolean isVerbose() {
		return verbose;
	}

	public void setVerbose(boolean verbose) {
		this.verbose = verbose;
	}

	public boolean isNoLink() {
		return noLink;
	}

	public void setNoLink(boolean noLink) {
		this.noLink = noLink;
	}

	public String getCheckOutIfExist() {
		return checkOutIfExist;
	}

	public void setCheckOutIfExist(String checkOutIfExist) {
		this.checkOutIfExist = checkOutIfExist;
	}

	public boolean getOverwriteIfExist() {
		return overwriteIfExist;
	}

	public void setOverwriteIfExist(boolean overwriteIfExist) {
		this.overwriteIfExist = overwriteIfExist;
	}

	public boolean getWriteSummary() {
		return writeSummary;
	}

}
