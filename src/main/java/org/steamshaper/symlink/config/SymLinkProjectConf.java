package org.steamshaper.symlink.config;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(Include.NON_NULL)  
public class SymLinkProjectConf {
	
	@JsonProperty(value = "project", required=true)
	private String projectName;
	
	@JsonProperty(value = "git-url", required=true)
	private String gitUrl;

	@JsonProperty(value = "git-branch", required=false)
	private String gitBranch =  "master";
	
	@JsonProperty(value = "checkout-folder")
	private String checkoutFolder;

	@JsonProperty(value = "auth")
	private SymLinkCredential auth;
	
	@JsonProperty(value = "pull-new-commits")
	private boolean pullCommits = false;



	public String getGitUrl() {
		return gitUrl;
	}


	public void setGitUrl(String gitUrl) {
		this.gitUrl = gitUrl;
	}


	public String getCheckoutFolder() {
		return checkoutFolder;
	}


	public void setCheckoutFolder(String checkoutFolder) {
		this.checkoutFolder = checkoutFolder;
	}


	public String getGitBranch() {
		return gitBranch;
	}


	public void setGitBranch(String gitBranch) {
		this.gitBranch = gitBranch;
	}


	public String getProjectName() {
		return projectName;
	}


	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}


	public SymLinkCredential getAuth() {
		return auth;
	}


	public void setAuth(SymLinkCredential auth) {
		this.auth = auth;
	}


	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("SymLinkProjectConf [");
		if (projectName != null)
			builder.append("projectName=").append(projectName).append(", ");
		if (gitUrl != null)
			builder.append("gitUrl=").append(gitUrl).append(", ");
		if (gitBranch != null)
			builder.append("gitBranch=").append(gitBranch).append(", ");
		if (checkoutFolder != null)
			builder.append("checkoutFolder=").append(checkoutFolder)
					.append(", ");
		if (auth != null)
			builder.append("auth=").append(auth);
		builder.append("]");
		return builder.toString();
	}


	public boolean isPullCommits() {
		
		return pullCommits;
	}


	public void setPullCommits(boolean pullCommits) {
		this.pullCommits = pullCommits;
	} 

}
