package org.steamshaper.symlink.config.external;

import java.util.Map;

import org.steamshaper.symlink.config.SymLinkCredential;
import org.steamshaper.symlink.config.SymLinkProxyConf;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(Include.NON_NULL)  
public class SymLinkExternalSettings {
	
	
	
	
	
	@Override
	public String toString() {
		return "SymLinkExternalSettings [credentialMap=" + credentialMap + ", defaultProxy=" + defaultProxy + "]";
	}


	@JsonProperty(value = "credential-map", required = false)
	private Map<String, SymLinkCredential> credentialMap;

	public Map<String, SymLinkCredential> getCredentialMap() {
		return credentialMap;
	}

	public void setCredentialMap(Map<String, SymLinkCredential> credentialMap) {
		this.credentialMap = credentialMap;
	}

	
	@JsonProperty(value="default-proxy",required=false)
	private SymLinkProxyConf defaultProxy;
	
	
	

	public SymLinkProxyConf getDefaultProxy() {
		return defaultProxy;
	}

	public void setDefaultProxy(SymLinkProxyConf defaultProxy) {
		this.defaultProxy = defaultProxy;
	}
	

}
