package org.steamshaper.symlink.config;

import java.util.List;
import java.util.Map;

import org.steamshaper.symlink.Constants;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(Include.NON_NULL)
public class SymLinkProfileConfig {
	
	@JsonProperty(value = "sources", required = true)
	private List<String> sourceRoots;
	
	@JsonProperty(value = "target", required=true)
	private String destinationPath = Constants.getDefaultDestDir();
	
	@JsonProperty(value = "marker-name", required=true)
	private String markerFile = Constants.getDefaultMarkerFileName();

	@JsonProperty(value = "projects", required = true)
	private Map<String, SymLinkProjectConf> projects;
	
	@JsonProperty(value = "proxy-map")
	private Map<String,SymLinkProxyConf> proxyMap;
	
	public List<String> getSourceRoots() {
		return sourceRoots;
	}

	public void setSourceRoots(List<String> sourceRoots) {
		this.sourceRoots = sourceRoots;
	}

	public String getDestinationPath() {
		return destinationPath;
	}

	public void setDestinationPath(String destinationRootPath) {
		this.destinationPath = destinationRootPath;
	}

	public String getMarkerFile() {
		return markerFile;
	}

	public void setMarkerFile(String markerFile) {
		this.markerFile = markerFile;
	}

	public Map<String, SymLinkProjectConf> getProjects() {
		return projects;
	}

	public void setProjects(Map<String, SymLinkProjectConf> projects) {
		this.projects = projects;
	}



	public Map<String, SymLinkProxyConf> getProxyMap() {
		return proxyMap;
	}

	public void setProxyMap(Map<String, SymLinkProxyConf> proxyMap) {
		this.proxyMap = proxyMap;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("SymLinkProfileConfig [");
		if (sourceRoots != null)
			builder.append("sourceRoots=").append(sourceRoots).append(", ");
		if (destinationPath != null)
			builder.append("destinationPath=").append(destinationPath)
					.append(", ");
		if (markerFile != null)
			builder.append("markerFile=").append(markerFile).append(", ");
		if (projects != null)
			builder.append("projects=").append(projects).append(", ");
		if (proxyMap != null)
			builder.append("proxyMap=").append(proxyMap);
		builder.append("]");
		return builder.toString();
	}

}
