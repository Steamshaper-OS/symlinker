package org.steamshaper.symlink.config;

import java.net.Proxy.Type;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)  
public class SymLinkProxyConf {

	@JsonProperty(value = "type")
	private Type type = Type.HTTP; // TO BE WRAPPED ??

	@JsonProperty(value = "address", required = true)
	private String address;

	@JsonProperty(value = "port", required = false)
	private Integer port;


	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String httpAddress) {
		this.address = httpAddress;
	}

	public Integer getPort() {
		return port;
	}

	public void setPort(Integer httpPort) {
		this.port = httpPort;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((address == null) ? 0 : address.hashCode());
		result = prime * result
				+ ((port == null) ? 0 : port.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SymLinkProxyConf other = (SymLinkProxyConf) obj;
		if (address == null) {
			if (other.address != null)
				return false;
		} else if (!address.equals(other.address))
			return false;
		if (port == null) {
			if (other.port != null)
				return false;
		} else if (!port.equals(other.port))
			return false;
		if (type != other.type)
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("SymLinkProxyConf [");
		if (type != null)
			builder.append("type=").append(type).append(", ");
		if (address != null)
			builder.append("httpAddress=").append(address).append(", ");
		if (port != null)
			builder.append("httpPort=").append(port);
		builder.append("]");
		return builder.toString();
	}


}
