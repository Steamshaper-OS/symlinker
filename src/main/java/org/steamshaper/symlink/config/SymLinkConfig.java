package org.steamshaper.symlink.config;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.steamshaper.symlink.Constants;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(Include.NON_NULL)  
public class SymLinkConfig {
	
	private SymLinkInfo symlink = SymLinkInfo.getInfos();

	@JsonProperty(value = "overwrite-if-exist", required=true)
	private Boolean overwriteIfExist = false;
	
	@JsonProperty(value = "work-directory", required=true)
	private String workDirectory;
	
	@JsonProperty(value = "sources", required=true)
	private List<String> sources = Constants.getDefaultSouceDirs();
	
	@JsonProperty(value = "target", required=true)
	private String target;
	
	@JsonProperty(value = "profiles", required=true)
	private Map<String,SymLinkProfileConfig> profiles = new HashMap<>();

	@JsonProperty(value = "projects", required = true)
	private Map<String, SymLinkProjectConf> projects;
	
	@JsonProperty(value = "properties")
	private Map<String, String> properties;
	
	
	

	@JsonProperty(value="default-proxy",required=false)
	private SymLinkProxyConf defaultProxy;
	
	
	public SymLinkInfo getSymlink() {
		return symlink;
	}

	public void setSymlink(SymLinkInfo symlink) {
		this.symlink = symlink;
	}

	public Boolean getOverwriteIfExist() {
		return overwriteIfExist;
	}

	public void setOverwriteIfExist(Boolean overwriteIfExist) {
		this.overwriteIfExist = overwriteIfExist;
	}

	public List<String> getSources() {
		return sources;
	}

	public void setSources(List<String> sourceRoots) {
		this.sources = sourceRoots;
	}

	public String getTarget() {
		return target;
	}

	public void setTarget(String destinationPath) {
		this.target = destinationPath;
	}

	public Map<String, SymLinkProfileConfig> getProfiles() {
		return profiles;
	}

	public void setProfiles(Map<String, SymLinkProfileConfig> profiles) {
		this.profiles = profiles;
	}

	public String getWorkDirectory() {
		return workDirectory;
	}

	public void setWorkDirectory(String workDirectory) {
		this.workDirectory = workDirectory;
	}

	public Map<String, SymLinkProjectConf> getProjects() {
		return projects;
	}

	public void setProjects(Map<String, SymLinkProjectConf> projects) {
		this.projects = projects;
	}


	public SymLinkProxyConf getDefaultProxy() {
		return defaultProxy;
	}

	public void setDefaultProxy(SymLinkProxyConf defaultProxy) {
		this.defaultProxy = defaultProxy;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("SymLinkConfig [");
		if (symlink != null)
			builder.append("symlink=").append(symlink).append(", ");
		if (overwriteIfExist != null)
			builder.append("overwriteIfExist=").append(overwriteIfExist)
					.append(", ");
		if (workDirectory != null)
			builder.append("workDirectory=").append(workDirectory).append(", ");
		if (sources != null)
			builder.append("sources=").append(sources).append(", ");
		if (target != null)
			builder.append("target=").append(target)
					.append(", ");
		if (profiles != null)
			builder.append("profiles=").append(profiles).append(", ");
		if (projects != null)
			builder.append("projects=").append(projects).append(", ");
		if (defaultProxy != null)
			builder.append("defaultProxy=").append(defaultProxy);
		builder.append("]");
		return builder.toString();
	}

	public Map<String, String> getProperties() {
		return properties;
	}

	public void setProperties(Map<String, String> properties) {
		this.properties = properties;
	}

}
