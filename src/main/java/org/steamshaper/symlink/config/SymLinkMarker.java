package org.steamshaper.symlink.config;

import java.io.File;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)  
public class SymLinkMarker {

	private String destinationPath;
	
	
	private String rename;
	
	@JsonIgnore
	private File markerFile;
	
	
	private boolean overwriteIfExist = false;
	
	public String getDestinationPath() {
		return destinationPath;
	}


	public void setDestinationPath(String destinationPath) {
		this.destinationPath = destinationPath;
	}

	public String getRename() {
		return rename;
	}


	public void setRename(String rename) {
		this.rename = rename;
	}


	public boolean getOverwriteIfExist() {
		return overwriteIfExist;
	}


	public void setOverwriteIfExist(boolean overwriteIfExist) {
		this.overwriteIfExist = overwriteIfExist;
	}


	public File getMarkerFile() {
		return markerFile;
	}


	public void setMarkerFile(File markerFile) {
		this.markerFile = markerFile;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("SymLinkMarker [");
		if (destinationPath != null)
			builder.append("destinationPath=").append(destinationPath)
					.append(", ");
		if (rename != null)
			builder.append("rename=").append(rename).append(", ");
		if (markerFile != null)
			builder.append("markerFile=").append(markerFile);
		builder.append("]");
		return builder.toString();
	}




	
	
	
	
	

}
