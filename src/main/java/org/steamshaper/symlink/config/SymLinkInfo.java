package org.steamshaper.symlink.config;

import org.steamshaper.symlink.Constants;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)  
public class SymLinkInfo {

	private String version = Constants.getVersion().getVersionNumber();

	private static final SymLinkInfo singleton = new SymLinkInfo();

	public static SymLinkInfo getInfos() {

		return singleton;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

}
